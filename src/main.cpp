#include <stdio.h>

/**
 * Entry point
 *
 * @param argc Arguments count
 * @param argv Arguments
 */
int main(int argc, char **argv) {
    printf("Hello, %s!\n", argv[1]);
}

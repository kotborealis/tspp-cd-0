# 🚀 CI & CD

## ⚒️ Необходимые инструменты

Для работы с Heroku необходим аккаунт, регистрируемся: https://signup.heroku.com/

Так же, для создания проекта используется `heroku-cli`: https://devcenter.heroku.com/articles/heroku-cli

Для генерации документации используем Doxygen.

## 📦 Создание проекта

Создадим файл `./src/main.cpp`, содержащий "логику" нашего приложения:
```c++
#include <stdio.h>

/**
 * Entry point
 *
 * @param argc Arguments count
 * @param argv Arguments
 */
int main(int argc, char **argv) {
    printf("Hello, %s!\n", argv[1]);
}
```

Программа принимает аргументы командой строки и выводит приветствие:
```shell
$ ./a.out
Hello, (null)!
$ ./a.out world
Hello, world!
$ ./a.out vega
Hello, vega!
```

В программе есть комментарии для генерации документации с помощью Doxygen.

Для компиляции используем Makefile:
```makefile
main:
	mkdir -p ./build/
	g++ ./src/main.cpp -o ./build/main
```

Исполняемый файл — `./build/main`.

Скомпилируем и запустим проект:
```shell
$ make
mkdir -p ./build/
g++ ./src/main.cpp -o ./build/main
$ ./build/main
Hello, world!
```

## 📚 Генерация документации

Для генерации документации используется Doxygen. Создадим файл конфигурации:
```doxyfile
# Doxyfile 1.8.13

# Имя проекта
PROJECT_NAME           = "tspp-cd"

# Язык документации
OUTPUT_LANGUAGE        = English

# Использовать C в качестве основного языка
OPTIMIZE_OUTPUT_FOR_C  = YES

# Входные файлы
INPUT                  = ./src/

# Выходные файлы
OUTPUT_DIRECTORY       = build

# Создать документацию для всех функций
EXTRACT_ALL            = YES

# Добавить список исходных файлов
SOURCE_BROWSER         = YES
``` 

Добавим новое задание в Makefile:
```
main:
	mkdir -p ./build/
	g++ ./src/main.cpp -o ./build/main
docs:
	mkdir -p ./build/
	doxygen Doxyfile
```

Сгенерируем документацию:
```shell
$ make docs
...
$ ls build/html
...
index.html
...
```

Откройте файл `index.html`, там находится сгенерированная документация:

![](https://s.micp.ru/IsiZI.png)

![](https://s.micp.ru/9rK26.png)

## 📸 Git

Добавим проект в систему контроля версий. Создадим файл `.gitignore` и перечислим игнорируемые файлы:
```
# Исключим папку build из репозитория
./build/
```

Добавим файлы в git:
```shell
$ git init
$ git add .
$ git commit -m "Initial commit"
```

Для CI/CD будем использовать GitLab. Добавим гитлаб в качестве удалённого репозитория:
```
$ git remote add origin https://gitlab.com/USERNAME/PROJECT_NAME
```

Отправим файлы на удалённый репозиторий:
```shell
$ git push -u origin master
```

## 🚀 GitLab CI

Настроим CI (Continious Integration) на основе Gitlab. Создадим файл `.gitlab-ci.yml`:
```yml
build:
    image: gcc
    stage: build
    before_script:
        - apt update && apt -y install make
    script:
        - make
    artifacts:
        paths:
            - ./build/main
```

Данный файл добавляет задание `build`. 
Секция `image` указывает, в каком окружении выполняется сборка. 
В данном случае используем окружение `gcc` для сборки C++.
Секция `stage` указывает этап, на котором выполняется задание.
В `script` указываются действия для сборки проекта.
Секция `before_script` выполняется перед `script`, и нужна для установки зависимостей
(в данном случае — для установки `make`).
Секция `artifacts` описывает "артефакты" — результаты сборки.
В ней указываем путь к исполняемому файлу.

Закоммитим и запушим изменения:
```shell
$ git add .gitlab-ci.yml
$ git commit -m "CI setup"
$ git push
```


### 🚀📚 Непрерывная интеграция документации

Добавим генерацию документации в настройку CI. 
В конец файла `.gitlab-ci.yml` добавим следующие строки:
```yml
docs:
    image: ubuntu:latest
    stage: build
    before_script:
        - apt update && apt -y install make doxygen
    script:
        - make docs
    artifacts:
        paths:
            - ./build/html/
```

Здесь добавляется задание `docs`, генерирующее документацию.
Всё то же самое, что для задания `build`, но другое окружение (`ubuntu` вместо `gcc`),
другие зависимости (добавился `doxygen`) и другие команды для сборки — собираем не программу, а документацию.
В `artifacts` прописываем пути до сгенерированных файлов.

### 🚀📚✨ Gitlab Pages

Gitlab Pages — встроенное средство для хранения и отображения сайтов, сгенерированных с помощью CI.
Подробнее — в документации: https://docs.gitlab.com/ee/user/project/pages/

Добавим в `.gitlab-ci.yml` задание `pages`:
```yml
pages:
    stage: deploy
    dependencies:
        - docs
    script:
        - mv ./build/html/ ./public/
    artifacts:
        paths:
            - public
```

Задание `pages` выполняется на этапе развёртывания (`stage: deploy`), идущим после `build`.
Здесь появилась новая секция — `dependencies`. 
В ней перечисляются зависимости задания — другие задания.
Задание `pages` не начнёт своё выполнение, пока не закончит своё выполнение задание `docs`,
 описанное в прерыдущем разделе.
Кроме того, артефакты, указанные в задании `docs`, будут доступны в задании `pages`.
В скрипте "сборки" переместим сгенерированные файлы документации в папку `public` и укажем её в артефактах,
в соответствии с требованиями Gitlab Pages.

При пуше на удалённый репозиторий выполнится CI:

![](https://s.micp.ru/6G904.png)

На скриншоте видно, что добавилось задание `pages:deploy`, не указанное в `.gitlab-ci.yml`.
Это внешнее (external) задание, добавленное автоматически гитлабом.
После выполнения CI/CD документация будет доступна по адресу https://USERNAME.gitlab.io/PROJECT_NAME .
**Примечание:** документация будет доступна не сразу, 
между первым развёртыванием и активацией gitlab pages может пройти около 30 минут.

![](https://s.micp.ru/9Nt3q.png)

## 🤖 Heroku

Предположим, что мы хотим развернуть наше приложение на удалённом сервере, чтобы оно было доступно из браузера.
Для этого воспользуемся решением Heroku — platform as a service (PaaS), представляющее
сервера для развёртывания приложений.
Бесплатная версия Heroku имеет некоторые ограничения (малое количество приватных приложений, 
после получаса приложение останавливается и должно быть приостановлено и т.д.), но его
достаточно для учебного процесса.

Для использования Heroku необходим heroku-cli. 
Сначала залогинимся командой `heroku login`, которая откроет браузер с подсказками для дальнейших действий.

В корне нашего проекта создадим новый проект Heroku:
```shell
$ heroku create
Creating app... done, ⬢ infinite-everglades-72800
https://infinite-everglades-72800.herokuapp.com/ | https://git.heroku.com/infinite-everglades-72800.git
```

Проект получил случайно сгенерированное имя `infinite-everglades-72800`.
Так же в выводе программы есть две ссылки: ссылка для доступа к приложению по HTTP и для 
деплоя кода по протоколу GIT.
С помощью ссылки на git мы можем отправлять код на деплой. `heroku` автоматически добавляет 
новый удалённый сервер гита:
Команда `git push heroku` запушит текущий код в Heroku, но развёртывание не произойдёт, т.к. 
мы не указали, как выполняется развёртывание.

Для описания развёртывания Heroku использует `buildpacks` — наборы команд,
выполняющие компиляцию программ и настройку окружения.

Для нашего проекта на C++ будем использовать `heroku-buildpack-c` — https://github.com/heroku/heroku-buildpack-c

Добавим `buildpack`:
```shell
$ heroku buildpacks:add https://github.com/heroku/heroku-buildpack-c
Buildpack added. Next release on infinite-everglades-72800 will use https://github.com/heroku/heroku-buildpack-c.
Run git push heroku master to create a new release using this buildpack.
```

При следующем деплое Heroku будет использовать указанный `buildpack`:
```shell
$ git push heroku
Counting objects: 4, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 400 bytes | 400.00 KiB/s, done.
Total 4 (delta 2), reused 0 (delta 0)
remote: Compressing source files... done.
remote: Building source:
remote:
remote: —--> C app detected
remote: —--> Compiling with Make
remote:        mkdir -p ./build/
remote:        g++ ./src/main.cpp -o ./build/main
remote: —--> Discovering process types
remote:        Procfile declares types -> (none)
remote:
remote: —--> Compressing...
remote:        Done: 6.2K
remote: —--> Launching...
remote:        Released v13
remote:        https://infinite-everglades-72800.herokuapp.com/ deployed to Heroku
remote:
remote: Verifying deploy... done.
To https://git.heroku.com/infinite-everglades-72800.git
   1e6aa93..3479d70  master -> master
```

`buildpack` нашёл `Makefile` в нашем проекте и выполнил сборку по умолчанию.

Выполним наше приложение на удалённом сервере:
```shell
$ heroku run ./build/main
Running ./build/main on ⬢ infinite-everglades-72800... up, run.1487 (Free)
Hello, world
```

Команда `heroku run` подключается к удалённому серверу и выполняет там указанную команду.
Как видим, наша программа выполнилась успешно.

### 🤖✨ Heroku WEB

Кроме адреса удалённого репозитория, Heroku выдал адрес http страницы — https://infinite-everglades-72800.herokuapp.com/

Мы можем запустить на удалённом сервере вебсервер, к которому будет предоставлен доступ по данному адресу.
В данный момент сраница выдаёт ошибку:

![](https://s.micp.ru/WPnw9.png)

Запустим команду `heroku logs --tail` и посмотрим сообщения об ошибках:
```shell
$ heroku logs --tail
2020-03-17T10:02:22.205753+00:00 heroku[router]: at=error code=H14 desc="No web processes running" method=GET path="/" host=infinite-everglades-72800.herokuapp.com request_id=6b3cd08f-a3f0-4178-95bd-abe4dc483aa8 fwd="94.228.207.176" dyno= connect= service= status=503 bytes= protocol=https
2020-03-17T10:02:22.524481+00:00 heroku[router]: at=error code=H14 desc="No web processes running" method=GET path="/favicon.ico" host=infinite-everglades-72800.herokuapp.com request_id=3494ddd5-d456-46f9-9792-ac12dfe37c4a fwd="94.228.207.176" dyno= connect= service= status=503 bytes= protocol=https
```

Логи говорят, что у нас нет процесса `web` для обслуживания запросов.

Для облегчения задачи, не будем писать вебсервер на C++, 
и запустим простой вебсервер на NodeJS,
который по запросу будет вызывать 
нашу программу `./build/main` и возвращать пользователю её вывод. 
Файл `./bin/httpd`:
```js
#!/usr/bin/env node

const cgi_bin = process.argv[2];
const port = process.argv[3];

require('http')
    .createServer((req, res) => {
		const args = req.url.slice(1).split('/');
        require('child_process')
			.execFile(cgi_bin, args, (err, stdout, stderr) => 
				{
					console.log(cgi_bin, args, err, stdout, stderr)
					res.end(stdout + stderr)
				}
			)
		}
	).listen(port)
```

Данный файл создаёт вебсервер, который будет вызывать команду, указанную в первом аргументе командной строки,
и будет обслуживать порт, указанный в третьем аргументе.
То есть, команда `./bin/httpd ./bin/main 80` будет обслуживать 80ый порт и вызывать программу `./build/main`.

При вызове программы данным сервером 
в качестве аргументов передаётся запрошенный URL, разбитый на массив по символу `/`
(
`https://infinite-everglades-72800.herokuapp.com/` -> ``,
`https://infinite-everglades-72800.herokuapp.com/hello` -> `hello`
`https://infinite-everglades-72800.herokuapp.com/path/to/some/file` -> `path`, `to`, `some`, `file
)

Далее, необходимо указать Heroku, какие команды надо вызвать для запуска приложения.
Создаим файл `Procfile`:
```
web: node ./bin/httpd ./build/main $PORT
```

Здесь указано, что мы создаём процесс с именем `web`, который для запуска вызывает
интерпретатор NodeJS `node`, выполняет файл нашего вебсервера `./bin/httpd`, и в качестве аргументов
передаёт ему `./build/main` и переменную окружения `$PORT`. 
Переменная `$PORT` устанавливается Heroku. Её необходимо использовать в качестве 
порта вебсервера для правильной работы вебприложения.

Так как мы используем NodeJS для вебсервера, необходимо добавить соответсвующий `buildpack`:
```shell
$ heroku buildpacks:add heroku/nodejs
Buildpack added. Next release on infinite-everglades-72800 will use:
  1. https://github.com/heroku/heroku-buildpack-c
  2. heroku/nodejs
Run git push heroku master to create a new release using these buildpacks.
```

Мы видим, что теперь у нас есть два `buildpack`'а. Первый используется для сборки приложения,
второй — для запуска вебсервера.

`buildpack` NodeJS для правильной работы требует наличие файла `package.json` в корне проекта. Создадим его:
```json
{}
```

Файл `package.json` содержит пустой JSON объект (`{}`) для правильной работы `buildpack`'а.

После этого мы можем закоммитить изменения и отправить их на сервер:
```shell
$ git add .
$ git commit -m "heroku"
$ git push heroku
Counting objects: 7, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (4/4), done.
Writing objects: 100% (7/7), 552 bytes | 184.00 KiB/s, done.
Total 7 (delta 2), reused 0 (delta 0)
remote: Compressing source files... done.
remote: Building source:
remote:
remote: —--> C app detected
remote: —--> Compiling with Make
remote:        mkdir -p ./build/
remote:        g++ ./src/main.cpp -o ./build/main
remote: —--> Node.js app detected
remote:
remote: —--> Creating runtime environment
remote:
remote:        NPM_CONFIG_LOGLEVEL=error
remote:        NODE_ENV=production
remote:        NODE_MODULES_CACHE=true
remote:        NODE_VERBOSE=false
remote:
remote: —--> Installing binaries
remote:        engines.node (package.json):  unspecified
remote:        engines.npm (package.json):   unspecified (use default)
remote:
remote:        Resolving node version 12.x...
remote:        Downloading and installing node 12.16.1...
remote:        Using default npm version: 6.13.4
remote:
remote: —--> Restoring cache
remote:        - node_modules (not cached - skipping)
remote:
remote: —--> Installing dependencies
remote:        Installing node modules (package.json)
remote:        up to date in 0.292s
remote:        found 0 vulnerabilities
remote:
remote:
remote: —--> Build
remote:
remote: —--> Caching build
remote:        - node_modules (nothing to cache)
remote:
remote: —--> Pruning devDependencies
remote:        up to date in 0.246s
remote:        found 0 vulnerabilities
remote:
remote:
remote: —--> Build succeeded!
remote: —--> Discovering process types
remote:        Procfile declares types -> web
remote:
remote: —--> Compressing...
remote:        Done: 22.1M
remote: —--> Launching...
remote:        Released v14
remote:        https://infinite-everglades-72800.herokuapp.com/ deployed to Heroku
remote:
remote: Verifying deploy... done.
To https://git.heroku.com/infinite-everglades-72800.git
   3479d70..a616989  master -> master
```

По логу видно, что у нас произошла сборка нашего проекта и настройка NodeJS для вебсервера.
Деплой прошёл успешно. Откроем страницу https://infinite-everglades-72800.herokuapp.com/ и увидим вывод
нашего приложения:

![](https://s.micp.ru/XZmkP.png)

Запрашивая разные URL, передаём программе разные аргументы:

![](https://s.micp.ru/6v4gY.png)

Таким образом, мы настроили доступ к программе на C++ через протокол HTTP, с помощью простого вебсервера на NodeJS.

## Gitlab ♥ Heroku

Последний шаг — настроим развёртывание приложения на heroku из Gitlab CI.
В файл `.gitlab-ci.yml` добавим строки:
```yml
production:
  image: ruby:latest
  only:
    - master
  stage: deploy
  script:
    - apt-get update -qy
    - apt-get install -y ruby-dev
    - gem install dpl
    - dpl --provider=heroku --app=$HEROKU_APP_NAME --api-key=$HEROKU_API_KEY
```

Всё то же самое, что и для остальных заданий, меняется только скрипт и окружение.
В скрипте устанавливаем зависимости для деплоя — `ruby` и утилиту `dpl`, 
позволяющую производить развёртывание на разные сервисы, включая `heroku`.
Последняя команда скрипта непосредственно выполняет развёртывание. Она использует две переменные 
окружения: `$HEROKU_APP_NAME` и `$HEROKU_API_KEY`. 
Эти переменные считаются секретными и задаются через настройки проекта гитлаба
(Settings — CI/CD — Variables):

![](https://s.micp.ru/V99YN.png)

В переменной `HEROKU_APP_NAME` укажем название нашего приложения: `infinite-everglades-72800`.
В переменной `HEROKU_API_KEY` указываем ключ для доступа к heroku (https://dashboard.heroku.com/account ):

![](https://s.micp.ru/9vYE8.png)

Пометим переменную `HEROKU_API_KEY` как protected и maksed, чтобы предотвратить её появление в логах —
**токен API является секретом**.

Не забываем сохранить переменные кнопкой *Save variables*.

Запушим новый файл `.gitlab-ci.yml` в гитлаб и посморим на состояние CI/CD:

![](https://s.micp.ru/85ca9.png)

Добавилось задание `production`, которое развёртывает наш код на Heroku.
Теперь, при каждем пуше в репозиторий, наш код будет разворачиваться на удалённом сервере Heroku.

# ✍️ Домашнее задание

1. Развернуть программу по данному описанию.
2. Модифицировать `main.cpp`:

Программа для записи и чтения файлов.

Если первый аргумент равен `write`, то записать в файл, указанный во втором аргументе строку, указанную в третьем аргументе.
При успешном выполнении вывести на экран `success`.
```shell
$ ./build/main write foo bar
success
```

Если первый аргумент равен `read`, то считать из файла, указанного во втором аргументе строку и вывести на экран.
Если указанный файл не существует, вывести `not found`.
```shell
$ ./build/main read foo
bar
$ ./build/main read totally_non_existing_file
not found
```

Если первый аргумент равен `delete`, удалить указанный файл. В любом случае вывести на экран `success`.
```shell
$ ./build/main delete foo
success
$ ./build/main delete totally_non_existing_file
success
```

Для упрощения задачи предположить, что все используемые строки всегда меньше 1024 символов.

3. Развернуть полученную программу на Heroku
4. Проверить, что реализованный функционал работает через веб 
( https://....herokuapp.com/write/foo/bar , 
https://....herokuapp.com/read/foo , 
https://....herokuapp.com/delete/foo ) работают так же, как в терминале.
5. Отправить в группу ВК ( https://vk.com/kmbalg18 ) ссылку на репозиторий и развёрнутое приложение.

***Срок до 25.03.2020.***
